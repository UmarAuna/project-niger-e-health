package buk.project.mini.e_health;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class Settings extends AppCompatActivity {
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    public static final String PREFS_NAME="myPrefsFile1";
    private TextView alarmTextView;
    TimePickerDialog timePickerDialog;
    Button setAlarm;
    Button offAlarm;
    final static int RQS_1=1000*60*60*24;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        alarmTextView = (TextView) findViewById(R.id.textAlarm);
        setAlarm = (Button) findViewById(R.id.Settimebutton);
        offAlarm = (Button)findViewById(R.id.offalarmbutton);
        alarmTextView = (TextView)findViewById(R.id.textAlarm);


        offAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alarmManager != null) {
                    alarmManager.cancel(pendingIntent);
                    Log.d("MyActivity", "Alarm Off");
                    alarmTextView.setVisibility(View.VISIBLE);
                    alarmTextView.setText("Notification is Off");
                    Toast.makeText(Settings.this, "Notification Off", Toast.LENGTH_SHORT).show();

                }
            }
        });


        setAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MyActivity","Notification on");
                openTimePickerDialog(false);

            }
        });
    }

    private void openTimePickerDialog(boolean is24r) {
        Calendar calendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(Settings.this, onTimeSetListener,
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), is24r);
        timePickerDialog.setIcon(R.drawable.call);
        timePickerDialog.setTitle("Alarm");
        timePickerDialog.show();
    }

    TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calnow = Calendar.getInstance();
            Calendar calset = (Calendar) calnow.clone();
            calset.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calset.set(Calendar.MINUTE, minute);
            calset.set(Calendar.SECOND, 0);
            calset.set(Calendar.MILLISECOND,0);

            if(hourOfDay < 12 && hourOfDay>=0){
                alarmTextView.setVisibility(View.VISIBLE);
                alarmTextView.setText(hourOfDay + ":"+ minute +"AM");
            }else{
                hourOfDay-=12;
                if(hourOfDay == 0){
                    hourOfDay = 12;
                }
                alarmTextView.setVisibility(View.VISIBLE);
                alarmTextView.setText(hourOfDay + " : "+minute+ " PM");
            }

            if (calset.compareTo(calnow) <= 0) {
                calset.add(Calendar.DATE, 1);
            }
            setAlarm(calset);
            Toast.makeText(Settings.this, "Notification On", Toast.LENGTH_SHORT).show();

        }
    };




    private  void setAlarm(Calendar targetCal){
        Intent myIntent = new Intent(Settings.this,AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(Settings.this,0,myIntent,0);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),RQS_1,pendingIntent);


    }

}
