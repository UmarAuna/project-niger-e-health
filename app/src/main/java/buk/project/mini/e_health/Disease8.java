package buk.project.mini.e_health;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Disease8 extends AppCompatActivity {
    public FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease8);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarD8);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fab = (FloatingActionButton) findViewById(R.id.fabD8);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerForContextMenu(fab);
                openContextMenu(fab);
                unregisterForContextMenu(fab);
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v ,menuInfo);
        menu.setHeaderTitle("Emergency Contacts");
        menu.setHeaderIcon(R.drawable.call);
        menu.add(0, v.getId(), 0 , "Ambulance");
        menu.add(0, v.getId(), 0 , "Police");
        menu.add(0, v.getId(), 0 , "Road Safety");
        menu.add(0, v.getId(), 0 , "Hospital");
    }

    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle() == "Ambulance"){
            Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
            try{
                startActivity(call);
            } catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
            }
        }else if(item.getTitle() == "Police"){
            Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
            try{
                startActivity(call);
            } catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
            }
        }else if(item.getTitle() == "Road Safety"){
            Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
            try{
                startActivity(call);
            } catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
            }
        }else if(item.getTitle() == "Hospital"){
            Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
            try{
                startActivity(call);
            } catch (android.content.ActivityNotFoundException ex){
                Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
            }
        } else{
            return false;
        }
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id==android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
