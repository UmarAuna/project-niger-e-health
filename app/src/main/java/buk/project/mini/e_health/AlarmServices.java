package buk.project.mini.e_health;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by UMAR AUNA on 29/08/2016.
 */
public class AlarmServices extends IntentService {

    private NotificationManager alarmNotificationManager;

    public AlarmServices(){
        super("AlarmService");
    }

    @Override
    public void onHandleIntent(Intent intent){
        sendNotification("Remember To use Mosquito Net to prevent you from Malaria");
    }
    private void sendNotification(String msg){
        Log.d("AlarmService", "Preparing to send notification....: "+msg);
        alarmNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0 ,new Intent(this,Settings.class),0);

        NotificationCompat.Builder alarmNotificationBuilder = new NotificationCompat.Builder(
                this).setContentTitle("E-Health").setSmallIcon(R.drawable.call).setStyle(new NotificationCompat.BigTextStyle()
                .bigText(msg)).setContentText(msg).setPriority(Notification.PRIORITY_MAX).setLights(Color.BLUE,500,500)
                .setVibrate(new long[]{1000,1000,1000,1000,1000});

        alarmNotificationBuilder.setContentIntent(contentIntent);
        alarmNotificationManager.notify(1,alarmNotificationBuilder.build());
        Log.d("AlarmService","Notification sent.");
    }

}
