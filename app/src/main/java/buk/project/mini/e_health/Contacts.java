package buk.project.mini.e_health;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

public class Contacts extends AppCompatActivity {
    CardView ambulance,police,road,hospital;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
         ambulance = (CardView)findViewById(R.id.ambulance);
         police = (CardView)findViewById(R.id.police);
         road = (CardView)findViewById(R.id.road);
         hospital = (CardView)findViewById(R.id.hospital);

        Ambulance();Police();Road_Safety();Hospital();
    }

    public void Ambulance(){
        ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
                try{
                    startActivity(call);
                } catch (android.content.ActivityNotFoundException ex){
                    Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Police(){
        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
                try{
                    startActivity(call);
                } catch (android.content.ActivityNotFoundException ex){
                    Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Road_Safety(){
       road.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
                try{
                    startActivity(call);
                } catch (android.content.ActivityNotFoundException ex){
                    Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Hospital(){
        hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call=new Intent(Intent.ACTION_CALL, Uri.parse("tel:+2347031162515"));
                try{
                    startActivity(call);
                } catch (android.content.ActivityNotFoundException ex){
                    Toast.makeText(getApplicationContext(), "Activity not founded", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
