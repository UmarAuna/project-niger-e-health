package buk.project.mini.e_health;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int Lokaci = 5000;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Timer gudu = new Timer();
        TimerTask showsplash = new TimerTask(){
            public void run(){
                finish();
                Intent nextIntent = new Intent (Splash.this,Lang_Choice.class);
                startActivity(nextIntent);
            }
        };
        gudu.schedule(showsplash,Lokaci);

    }
}
