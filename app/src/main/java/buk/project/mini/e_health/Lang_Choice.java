package buk.project.mini.e_health;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Lang_Choice extends AppCompatActivity {
    Button English,Hausa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lang__choice);
        English = (Button)findViewById(R.id.Eng);
        Hausa = (Button)findViewById(R.id.Hau);

        Lang_English();
        Lang_Hausa();
    }

    public void Lang_English(){
        English.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent ENG = new Intent(Lang_Choice.this,MainActivity.class);
                startActivity(ENG);
            }
        });
    }

    public void Lang_Hausa(){
        Hausa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
