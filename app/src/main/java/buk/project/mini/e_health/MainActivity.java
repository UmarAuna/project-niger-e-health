package buk.project.mini.e_health;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    CardView Disease1, Disease2, Disease3, Disease4, Disease5, Disease6, Disease7, Disease8, Disease9, Disease10, Check_health;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Disease1 = (CardView) findViewById(R.id.card1);
        Disease2 = (CardView) findViewById(R.id.card2);
        Disease3 = (CardView) findViewById(R.id.card3);
        Disease4 = (CardView) findViewById(R.id.card4);
        Disease5 = (CardView) findViewById(R.id.card5);
        Disease6 = (CardView) findViewById(R.id.card6);
        Disease7 = (CardView) findViewById(R.id.card7);
        Disease8 = (CardView) findViewById(R.id.card8);
        Disease9 = (CardView) findViewById(R.id.card9);
        Disease10 = (CardView) findViewById(R.id.card10);
        Check_health = (CardView) findViewById(R.id.card11);

        D1();
        D2();
        D3();
        D4();
        D5();
        D6();
        D7();
        D8();
        D9();
        D10();
        C();

    }

    public void D1() {
        Disease1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D1 = new Intent(MainActivity.this, Disease1.class);
                startActivity(D1);
            }
        });
    }

    public void D2() {
        Disease2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D2 = new Intent(MainActivity.this, Disease2.class);
                startActivity(D2);
            }
        });
    }

    public void D3() {
        Disease3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D3 = new Intent(MainActivity.this, Disease3.class);
                startActivity(D3);
            }
        });
    }

    public void D4() {
        Disease4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D4 = new Intent(MainActivity.this, Disease4.class);
                startActivity(D4);
            }
        });
    }

    public void D5() {
        Disease5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D5 = new Intent(MainActivity.this, Disease5.class);
                startActivity(D5);
            }
        });
    }

    public void D6() {
        Disease6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D6 = new Intent(MainActivity.this, Disease6.class);
                startActivity(D6);
            }
        });
    }

    public void D7() {
        Disease7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D7 = new Intent(MainActivity.this, Disease7.class);
                startActivity(D7);
            }
        });
    }

    public void D8() {
        Disease8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D8 = new Intent(MainActivity.this, Disease8.class);
                startActivity(D8);
            }
        });
    }

    public void D9() {
        Disease9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D9 = new Intent(MainActivity.this, Disease9.class);
                startActivity(D9);
            }
        });
    }

    public void D10() {
        Disease10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent D10 = new Intent(MainActivity.this, Disease10.class);
                startActivity(D10);
            }
        });
    }

    public void C() {
        Check_health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent C = new Intent(MainActivity.this, Check_Health.class);
                startActivity(C);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_disease1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.about) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setTitle("E-Health");
            alertbox.setIcon(R.drawable.call);
            alertbox.setMessage("My Name is Umar Saidu Auna and I Love to Program");
            alertbox.setCancelable(false);
            alertbox.setNeutralButton("", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertbox.setNegativeButton("Close", null);
            alertbox.show();
            return true;
        } else if (id == R.id.emergency) {
            Intent gonext = new Intent(MainActivity.this, Contacts.class);
            startActivity(gonext);
            return true;
        } else if (id == R.id.time) {
            Intent next = new Intent(MainActivity.this, Settings.class);
            startActivity(next);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}