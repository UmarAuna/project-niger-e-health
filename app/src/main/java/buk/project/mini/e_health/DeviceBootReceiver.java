package buk.project.mini.e_health;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

/**
 * Created by UMAR AUNA on 29/08/2016.
 */
public class DeviceBootReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent){
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            Intent alarmIntent = new Intent(context,AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,alarmIntent,0);
            AlarmManager manager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
            int interval = 8000;
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval,pendingIntent);
            Toast.makeText(context,"E-health Alarm is on", Toast.LENGTH_SHORT).show();
        }
    }
}
